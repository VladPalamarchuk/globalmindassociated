package kiev.palamarchuk.globalmindassociated;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import adapters.AdapterExpandableCar;
import interfaces.IUserAuthorization;
import utils.SampleJSON;


public class MainActivity extends ActionBarActivity implements IUserAuthorization {

    private ExpandableListView LVExpandable;
    private AdapterExpandableCar adapterExpandableCar;

    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        adapterExpandableCar = new AdapterExpandableCar(this);

        LVExpandable = (ExpandableListView) findViewById(R.id.activityMainExpandableListView);
        LVExpandable.setAdapter(adapterExpandableCar);

        LVExpandable.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (parent.getExpandableListAdapter().getChildrenCount(groupPosition) == 0) {
                    // this items not contains children, so toast message does not need here
                    return true;
                }
                boolean authorized = isAuthorized();
                if (!authorized) {
                    toast(R.string.authorization_needed);
                }
                return !authorized;
            }
        });


        try {
            final JSONArray carsWithDetails = SampleJSON.getInstance().create();
            final int length = carsWithDetails.length();

            adapterExpandableCar.setCarsJSONArray(carsWithDetails);

            LVExpandable.postDelayed(new Runnable() {
                @Override
                public void run() {
                    LVExpandable.smoothScrollToPositionFromTop(length - 1, 500, length * 2000);
                }
            }, 500);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (!isAuthorized()) {
            new AlertDialog.Builder(this).setTitle(R.string.authorization_needed).setPositiveButton(R.string.authorize, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    authorize();
                }
            }).setNegativeButton(R.string.late, null).show();
        }

        updateTitleAndMenuAndExpandableList();
    }


    @Override
    public boolean isAuthorized() {
        return prefs.getBoolean(KEY_AUTHORIZED, false);
    }

    @Override
    public void changeStatus(boolean authorized) {
        prefs.edit().putBoolean(KEY_AUTHORIZED, authorized).apply();
        updateTitleAndMenuAndExpandableList();
    }

    @Override
    public void authorize() {
        changeStatus(true);
        toast(R.string.authorization_success);
    }

    @Override
    public void leave() {
        new AlertDialog.Builder(this).setTitle(R.string.question_leave_account).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                changeStatus(false);
            }
        }).setNegativeButton(android.R.string.cancel, null).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.action_auth).setTitle(
                isAuthorized() ? R.string.sign_out : R.string.sign_in
        );
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_auth:
                if (isAuthorized()) {
                    leave();
                } else {
                    authorize();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateTitleAndMenuAndExpandableList() {
        if (isAuthorized()) {
            getSupportActionBar().setTitle(R.string.you_are_authorized);
        } else {
            getSupportActionBar().setTitle(R.string.authorization_needed);

            /**
             * new ArrayList constructor used to avoid
             * @see java.util.ConcurrentModificationException
             */
            for (Integer expandedGroupPosition : new ArrayList<>(adapterExpandableCar.getExpandedItemsPositions())) {
                LVExpandable.collapseGroup(expandedGroupPosition);
            }
        }

        invalidateOptionsMenu();
    }

    private void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void toast(int message) {
        toast(getString(message));
    }
}
