package utils;


import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

import interfaces.JSONKeys;

public class SampleJSON {

    private static final String TAG = SampleJSON.class.getSimpleName();

    private static final String[][] sampleData = {
            {"Легковой", "BMV", "Черный", "new"},
            {"Легковой", "Lanos", "Blue", "б/у"},
            {"Грузовой", "КРАЗ", "Ржавый", "новый"},
            {"Лодка", "Titanic", "зеленый", "б/у"},
            {"Самолет", "AIR 1", "Черный", "непонятно"},
            {"Car type 1", "mark1", "color 1", "state 1"},
            {"Легковой", "BMV", "Черный", "new"},
            {"Легковой", "Lanos", "Blue", "б/у"},
            {"Грузовой", "КРАЗ", "Ржавый", "новый"},
            {"Лодка", "Titanic", "зеленый", "б/у"},
            {"Самолет", "AIR 1", "Черный", "непонятно"},
            {"Car type 1", "mark1", "color 1", "state 1"},
            {"Легковой", "BMV", "Черный", "new"},
            {"Легковой", "Lanos", "Blue", "б/у"},
            {"Грузовой", "КРАЗ", "Ржавый", "новый"},
            {"Лодка", "Titanic", "зеленый", "б/у"},
            {"Самолет", "AIR 1", "Черный", "непонятно"},
            {"Car type 1", "mark1", "color 1", "state 1"},
            {"Легковой", "BMV", "Черный", "new"},
            {"Легковой", "Lanos", "Blue", "б/у"},
            {"Грузовой", "КРАЗ", "Ржавый", "новый"},
            {"Лодка", "Titanic", "зеленый", "б/у"},
            {"Самолет", "AIR 1", "Черный", "непонятно"},
            {"Car type 1", "mark1", "color 1", "state 1"},
            {"Легковой", "BMV", "Черный", "new"},
            {"Легковой", "Lanos", "Blue", "б/у"},
            {"Грузовой", "КРАЗ", "Ржавый", "новый"},
            {"Лодка", "Titanic", "зеленый", "б/у"},
            {"Самолет", "AIR 1", "Черный", "непонятно"},
            {"Car type 1", "mark1", "color 1", "state 1"},
            {"Легковой", "BMV", "Черный", "new"},
            {"Легковой", "Lanos", "Blue", "б/у"},
            {"Грузовой", "КРАЗ", "Ржавый", "новый"},
            {"Лодка", "Titanic", "зеленый", "б/у"},
            {"Самолет", "AIR 1", "Черный", "непонятно"},
            {"Car type 1", "mark1", "color 1", "state 1"},
    };
    private static final String[][] sampleDataDetails = {
            {"Anton", "+380938141741"},
            {"Mark", "+380974941231"},
            {"Ostap", "+380954941231"},
            {"Somebody", "+380374941231"},
            {"Vladislav", "+380975641231"}
    };
    private static Random random = new Random();

    private static SampleJSON ourInstance = new SampleJSON();

    public static SampleJSON getInstance() {
        return ourInstance;
    }

    private SampleJSON() {
    }

    public JSONArray create() throws JSONException {
        JSONArray jsonArray = new JSONArray();

        for (int i = sampleData.length - 1; i >= 0; i--) {
            JSONObject car = createCarLine(sampleData[i][0], sampleData[i][1], sampleData[i][2], sampleData[i][3]);

            if (i % 2 == 0) {
                int randomCarDetailPosition = random.nextInt(5);
                car.put(JSONKeys.details, createDetails(sampleDataDetails[randomCarDetailPosition][0], sampleDataDetails[randomCarDetailPosition][1]));
            }
            jsonArray.put(car);
        }
        Log.d(TAG, "generated json: \n" + jsonArray.toString(3));
        return jsonArray;
    }

    private JSONObject createCarLine(String type, String mark, String color, String state) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSONKeys.type, type);
        jsonObject.put(JSONKeys.mark, mark);
        jsonObject.put(JSONKeys.color, color);
        jsonObject.put(JSONKeys.state, state);

        return jsonObject;
    }

    private JSONObject createDetails(String name, String phone) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSONKeys.name, name);
        jsonObject.put(JSONKeys.phone, phone);
        return jsonObject;
    }

}
