package interfaces;


public interface IUserAuthorization {

    String KEY_AUTHORIZED = "KEY_AUTHORIZED";

    boolean isAuthorized();

    void changeStatus(boolean authorized);

    void authorize();

    void leave();
}
