package interfaces;


public interface JSONKeys {
    String type = "type";
    String mark = "mark";
    String color = "color";
    String state = "state";

    String details = "details";

    String name = "name";
    String phone = "phone";
}
