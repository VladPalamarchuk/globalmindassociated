package adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import interfaces.IUserAuthorization;
import interfaces.JSONKeys;
import kiev.palamarchuk.globalmindassociated.MainActivity;
import kiev.palamarchuk.globalmindassociated.R;


public class AdapterExpandableCar extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<Integer> expandedItemsPositions;

    public ArrayList<Integer> getExpandedItemsPositions() {
        return expandedItemsPositions;
    }

    public AdapterExpandableCar(Context context) {
        this.context = context;
        this.expandedItemsPositions = new ArrayList<>();
    }


    private JSONArray carsJSONArray;

    public void setCarsJSONArray(JSONArray carsJSONArray) {
        this.carsJSONArray = carsJSONArray;
    }

    //|тип транспорта|марка|цвет|состояние|
    @Override
    public int getGroupCount() {
        return carsJSONArray != null ? carsJSONArray.length() : 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return carsJSONArray.optJSONObject(groupPosition).has(JSONKeys.details) ? 1 : 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return carsJSONArray.optJSONObject(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return carsJSONArray.optJSONObject(groupPosition).optJSONObject(JSONKeys.details);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        JSONObject carInfo = (JSONObject) getGroup(groupPosition);
        GroupHolder holder;

        if (convertView == null) {
            holder = new GroupHolder();
            convertView = View.inflate(context, R.layout.row_expand_car_list, null);

            holder.TVType = (TextView) convertView.findViewById(R.id.rowExpandCarListType);
            holder.TVMark = (TextView) convertView.findViewById(R.id.rowExpandCarListMark);
            holder.TVColor = (TextView) convertView.findViewById(R.id.rowExpandCarListColor);
            holder.TVState = (TextView) convertView.findViewById(R.id.rowExpandCarListState);

            holder.IVNext = (ImageView) convertView.findViewById(R.id.rowExpandCarListNextIndicator);

            convertView.setTag(holder);
        }

        holder = (GroupHolder) convertView.getTag();

        holder.TVType.setText(carInfo.optString(JSONKeys.type));
        holder.TVMark.setText(carInfo.optString(JSONKeys.mark));
        holder.TVColor.setText(carInfo.optString(JSONKeys.color));
        holder.TVState.setText(carInfo.optString(JSONKeys.state));

        holder.IVNext.setVisibility(getChildrenCount(groupPosition) != 0 ? View.VISIBLE : View.INVISIBLE);
        holder.IVNext.setRotation(expandedItemsPositions.contains(groupPosition) ? 90F : 0F);

        return convertView;
    }


    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        JSONObject details = (JSONObject) getChild(groupPosition, childPosition);
        DetailsHolder holder;
        if (convertView == null) {
            holder = new DetailsHolder();

            convertView = View.inflate(context, R.layout.row_expand_car_list_details, null);

            holder.TVName = (TextView) convertView.findViewById(R.id.rowExpandCarListDetailsName);
            holder.TVPhone = (TextView) convertView.findViewById(R.id.rowExpandCarListDetailsPhone);

            convertView.setTag(holder);
        }

        holder = (DetailsHolder) convertView.getTag();

        holder.TVName.setText(details.optString(JSONKeys.name));
        holder.TVPhone.setText(details.optString(JSONKeys.phone));

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        expandedItemsPositions.remove(Integer.valueOf(groupPosition));
        notifyDataSetChanged();
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        expandedItemsPositions.add(groupPosition);
        notifyDataSetChanged();
    }


    private class GroupHolder {
        TextView TVType;
        TextView TVMark;
        TextView TVColor;
        TextView TVState;

        ImageView IVNext;

    }

    private class DetailsHolder {
        TextView TVName;
        TextView TVPhone;
    }
}
